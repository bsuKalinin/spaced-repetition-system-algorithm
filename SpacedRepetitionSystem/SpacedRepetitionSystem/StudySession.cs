﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SpacedRepetitionSystem
{
    public class StudySession<T> : IEnumerable<T>
       where T : IReviewItem, new()
    {
        private readonly IEnumerator<T> _enumerator;
        private readonly List<T> _revisionList = new List<T>();
        private int _newCardsReturned;
        private int _existingCardsReturned;


        public IReviewAlgorithm ReviewAlgorithm { get; set; }
        public int MaxNewCards { get; set; }
        public int MaxExistingCards { get; set; }


        public StudySession(IEnumerable<T> items, IReviewAlgorithm reviewAlgorithm)
        {
            _enumerator = items.GetEnumerator();

            ReviewAlgorithm = reviewAlgorithm;
            MaxNewCards = 25;
            MaxExistingCards = 100;
        }

        public T Review(T item, ReviewResult outcome)
        {
            _revisionList.Remove(item);

            var nextReview = new T();

            if (outcome != ReviewResult.Incorrect & outcome!= ReviewResult.NeverReviewed)
            {
                nextReview.CorrectReviewStreak = item.CorrectReviewStreak + 1;
                nextReview.PreviousCorrectReview = item.ReviewDate;

            }

            else
            {
                nextReview.CorrectReviewStreak = 0;
                nextReview.PreviousCorrectReview = DateTime.MinValue;

                _revisionList.Add(nextReview);
            }

            nextReview.ReviewDate = DateTime.Now;
            nextReview.DifficultyRating = ReviewAlgorithm.AdjustDifficulty(item, outcome);
            nextReview.ReviewResult = outcome;

            return nextReview;
        }

        public IEnumerator<T> GetEnumerator()
        {
            while (_enumerator.MoveNext())
            {
                var item = _enumerator.Current;

                if (IsDue(item))
                {
                    if (IsNewItem(item))
                    {
                        _newCardsReturned++;

                        if (_newCardsReturned <= MaxNewCards)
                        {
                            yield return item;
                        }
                    }

                    else
                    {
                        _existingCardsReturned++;

                        if (_existingCardsReturned <= MaxExistingCards)
                        {
                            yield return item;
                        }
                    }
                }
            }

            while (_revisionList.Count > 0)
            {
                yield return _revisionList[0];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private bool IsDue(IReviewItem item)
        {
            var nextReview = ReviewAlgorithm.NextReview(item, 6);
            return nextReview <= DateTime.Now;
        }

        private static bool IsNewItem(IReviewItem item)
        {
            return item.ReviewDate == DateTime.MinValue;
        }

        public DateTime IsDuePublic(IReviewItem item, int days)
        {
            return ReviewAlgorithm.NextReview(item, days);
        }
    }
}
