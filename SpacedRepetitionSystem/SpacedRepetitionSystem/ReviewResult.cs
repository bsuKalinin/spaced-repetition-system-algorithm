﻿namespace SpacedRepetitionSystem
{
    public enum ReviewResult
    {
        NeverReviewed,
        Incorrect,
        Uncertain,
        Perfect
    }
}
