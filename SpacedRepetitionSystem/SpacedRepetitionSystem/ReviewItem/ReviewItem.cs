﻿using System;

namespace SpacedRepetitionSystem
{
    public class ReviewItem : IReviewItem
    {
        public string English { get; set; }
        public string Japanese { get; set; }

        public string Description { get; set; }
        public int CorrectReviewStreak { get; set; }
        public DateTime ReviewDate { get; set; }
        public DateTime PreviousCorrectReview { get; set; }
        public DifficultyRating DifficultyRating { get; set; }
        public ReviewResult ReviewResult { get; set; }

        public ReviewItem()
        {
            if (DifficultyRating is null)
            {
                DifficultyRating = 0;
            }
        }

        public override string ToString()
        {
            return $"{Description}\nCorrect review streak is {CorrectReviewStreak}\nPrevious correct review is {PreviousCorrectReview}\nReview date is {ReviewDate}\n";
        }
    }
}
