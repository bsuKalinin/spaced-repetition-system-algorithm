﻿using System;

namespace SpacedRepetitionSystem
{
	public class SuperMemo2 : IReviewAlgorithm
	{
        public DateTime NextReview(IReviewItem item, int days)
        {
            var now = DateTime.Now;

            if (item.CorrectReviewStreak == 0)
            {
                return now;
            }

            if (item.CorrectReviewStreak == 1)
            {
                return item.ReviewDate.AddDays(days);
            }

            var EF = DifficultyRatingToEF(item.DifficultyRating.Percentage);
            var daysSincePreviousReview = (item.ReviewDate - item.PreviousCorrectReview).Days;
            var daysUntilNextReview = (daysSincePreviousReview - 1) * EF;

            return item.ReviewDate.AddDays(daysUntilNextReview);
        }

        public DifficultyRating AdjustDifficulty(IReviewItem item, ReviewResult result)
        {
            //EF':=EF+(0.1-(5-q)*(0.08+(5-q)*0.02))
            //where:
            //EF' - new value of the E-Factor,
            //EF - old value of the E-Factor,
            //q - quality of the response in the 0-3 grade scale.

            var currentEF = DifficultyRatingToEF(item.DifficultyRating.Percentage);
            var newEF = currentEF + (0.1 - (3 - (int)result) * (0.08 + (3 - (int)result) * 0.02));
            var newDifficultyRating = EFToDifficultyRating(newEF);

            newDifficultyRating = newDifficultyRating > 100 ? 100 : newDifficultyRating;
            newDifficultyRating = newDifficultyRating < 0 ? 0 : newDifficultyRating;

            return new DifficultyRating(newDifficultyRating);
        }

        public double DifficultyRatingToEF(int difficultyRating)
        {
            return (-0.012 * difficultyRating) + 2.5;
        }

        public int EFToDifficultyRating(double ef)
        {
            return (int)((ef - 2.5) / -0.012);
        }
    }
}
