﻿using System;

namespace SpacedRepetitionSystem
{
	public interface IReviewAlgorithm
	{
		//Calculates next day u need to review this item
		DateTime NextReview(IReviewItem item, int days);

		DifficultyRating AdjustDifficulty(IReviewItem item, ReviewResult outcome);
	}
}
