﻿using System;

namespace SpacedRepetitionSystem
{
	class Program
	{
		public static void Main(string[] args)
		{
			var studySession = new StudySession<ReviewItem>(new ReviewItem[]{
				new ReviewItem()
			}, new SuperMemo2());
			Console.WriteLine("Please, use only correct symbols, this is just a demo, and there is no any checkers");
			bool cont = true;
			do
			{
				Console.WriteLine("There is only one parameter which  affects on next review date without user answer, if user's correct answer streak is 1, we always give 6 days for next review, but u can change it: ");
				Console.WriteLine("Enter days for next review, if his correct streak is 1 ");
				int days = int.Parse(Console.ReadLine());
				Console.WriteLine("Enter month of previous correct review (1 - 12, without '0')");
				int monthPrevCorrectDate = int.Parse(Console.ReadLine());
				Console.WriteLine("Enter day of previous correct review (1 - 31, without '0' and correct day for correct month)");
				int dayPrevCorrectDate = int.Parse(Console.ReadLine());
				Console.WriteLine("Enter difficulty rating (string similarity alg, value from 0 to 100)");
				int difficultyRating = int.Parse(Console.ReadLine());
				Console.WriteLine("Enter correct review streak (from 0 to inf) ");
				int correctRevStreak = int.Parse(Console.ReadLine());
				ReviewItem review = new ReviewItem()
				{
					English = "Word",
					Japanese = "droW",
					DifficultyRating = new DifficultyRating(100 - difficultyRating),
					ReviewDate = DateTime.Now,
					PreviousCorrectReview = new DateTime(2020, monthPrevCorrectDate, dayPrevCorrectDate),
					CorrectReviewStreak = correctRevStreak
				};
				Console.WriteLine(review.ToString());
				Console.WriteLine("next review date is " + studySession.IsDuePublic(review, days));
				Console.WriteLine("Would u like to try again? Enter 'y'/'n'");
				string answer = Console.ReadLine();
				cont = answer.Equals("y");
			}
			while (cont);

			Console.WriteLine("Endzzzzzzzzzzzzzzzzz");
			
		}
	}
}
