﻿using System;

namespace SpacedRepetitionSystem
{
    public class DifficultyRating
    {
        public int Percentage { get; private set; }

        public DifficultyRating(int percentage)
        {
            if (percentage < 0 || percentage > 100)
            {
                throw new ArgumentOutOfRangeException("percentage", "DifficultyRating be between 0 (least difficult) and 100 (most difficult)");
            }

            Percentage = percentage;
        }

        public static implicit operator int(DifficultyRating rating)
        {
            return rating.Percentage;
        }

        public static implicit operator DifficultyRating(int percentage)
        {
            return new DifficultyRating(percentage);
        }
    }
}
